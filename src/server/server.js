const express = require('express')
const app = express()
const server = require('http').createServer(app)
const io = require('socket.io')(server)
const request = require('request')


app.use('/',express.static('../public'))

io.on('connection',(socket)=>{
    let timeoutEmit = 5000
    
    setInterval(async ()=> {
        socket.emit('dataFromQuery', socket.id)
        console.log(`User connected ID = ${socket.id}`)
        // request('http://test-uccx01.nexentel.com:9080/realtime/ResourceIAQStats',(err,res,body)=>{
        //     if(err) console.error('error',err)
        //     console.log('request to uccx api statusCode = ',res && res.statusCode)
        //     io.emit('dataFromQuery', body)
        // })
    }, timeoutEmit)
    
    socket.on('disconnect',()=>{
        console.log(`User disconnected, ID = ${socket.id}`)
    })
})


server.listen(8000,()=>console.log('server running...'))