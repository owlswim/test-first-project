import './App.css'
import React from 'react';
import Clock from './components/Clock';
// import { BasicPie } from './components/PieChart';

const App = () => {
	return (
		<div className='app'>
			<header className='app-header'>
				<div className='app-header-logo'><img src='../public/images/kerry-logo.png' alt='kerry-logo.png'/></div>
				
				<div className='app-header-clock'><Clock/></div>
			</header>

            <div className='allteam'>
                <div className='team'>
					<div className='box-1'>Team KERRY Express</div>
					<div className='box-2'>
						<div className='box-data'>call presented<br/>xxxx</div>
						<div className='box-data'>call waiting<br/>xxxx</div>
					</div>
					<div className='box-2'>
						<div className='box-data'>call presented<br/>xxxx</div>
						<div className='box-data'>call waiting<br/>xxxx</div>
					</div>
					<div className='box-2'>
						<div className='box-data'>call presented<br/>xxxx</div>
						<div className='box-data'>call waiting<br/>xxxx</div>
					</div>
					<div className='box-2'>
						<div className='box-data'>call presented<br/>xxxx</div>
						<div className='box-data'>call waiting<br/>xxxx</div>
					</div>
					<div className='box-1'>Graph</div>
					<div className='box-1'>Graph</div>
				</div>

				<div className='team'>Team</div>
				<div className='team'>Team</div>
				<div className='team'>Team</div>
            </div>


		</div>
	);
};

export default App;
