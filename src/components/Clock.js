import React from "react";
import './Clock.css'

class Clock extends React.Component{

    constructor(prop){
        super(prop)
        this.state = {
            date: new Date()
        }
        console.log(prop.params)

        this.tick = this.tick.bind(this)
    }


    tick(){
        this.setState(()=>{
            return {
                date: new Date()
            }
        })
    }

    componentDidMount(){
        this.timerId = setInterval(()=>{
            this.tick()
        },1000)
    }

    componentWillUnmount(){
        clearInterval(this.timerId)
    }

    render(){

        return (
            <div className="clock">
                {this.state.date.toLocaleDateString('th-TH',{dateStyle: "medium"})}<br/>
                {this.state.date.toLocaleTimeString('short')}
            </div>
        )
    }
}

export default Clock